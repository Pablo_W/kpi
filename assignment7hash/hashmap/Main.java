package hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Create an empty hash map
		HashMap<String, Integer> fruit = new HashMap<>();
		
		// Add fruits in fruit hash map
		fruit.put("apple", 1);
		fruit.put("banana", 2);
		fruit.put("grapefruit", 3);
		fruit.put("orange", 4);
		
		// Print size and content
		System.out.println("The size of fruit hash map is :- " 
				+ fruit.size());
		System.out.println(fruit);
		
		// isEmpty 
        System.out.println("Whether the fruit hash map is empty? : " 
        		+ fruit.isEmpty());

        // Remove key"apple" from the map and check
        fruit.remove("apple");
        System.out.println("Whether the fruit hash map contains \"apple\"? : " 
        		+ fruit.containsKey("apple"));
        
        // Use keySet to get fruit hash map
        Set<String> set = fruit.keySet();
        for(String key: set) {
        	System.out.println(key+" " + fruit.get(key));
        }
        
        // Use entrySet to get fruit hash map
        for(Map.Entry<String, Integer> entry : fruit.entrySet()) {
        	System.out.println("Key=" + entry.getKey() + " Value=" + entry.getValue());
        }
	}

}
