package hashtable;

public class HashTable {
	private final int HASHSIZE = 12;
	private final int NULLKEY = -7766;
	private int[] arr = new int[HASHSIZE];
	private int size = 0;
	
	//Insert key to the HashTable
	public void put(int key) {
		int i = 0;
		if(size == 0) {
			InitHashTable();
		}
		while(arr[Hash(key+i)] != NULLKEY) {
			i++;
		}
		arr[Hash(key)+i] = key;
		size++;
	}
	
	// Show the HashTable
	public void show() {
		for(int i=0; i<HASHSIZE; i++) {
			if(size == 0) {
				System.out.println("The HashTable is empty.");
				break;
			}
			else if(arr[i] != NULLKEY) {
				System.out.println("key:" + arr[i] + " location:" + i);
			}
		}
	}
	
	// Get the key according the number that user input
	public int get(int number) {
		return arr[number];
	}
	
	// Clear the HashTable
	public void clear() {
		InitHashTable();
		size =0;
	}
	
	// Get the size of the HashTable
	public int size() {
		return size;
	}
	
	// Initialize the hash table
	private void InitHashTable() {
		for(int i = 0; i<HASHSIZE; i++) {
			arr[i] = NULLKEY;
		}
	}
	
	// Mod method for value
	private int Hash(int key) {
		return key % HASHSIZE;
	}
}
