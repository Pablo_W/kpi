package assignment6;

public class Node {
	public int data;
	public Node next;
	public Node previous;
	
	public Node(int data) {
		this.data = data;
	}
	public Node(int data, Node next ,Node previous) {
		this.data = data;
		this.next = next;
		this.previous = previous;
	}

}
