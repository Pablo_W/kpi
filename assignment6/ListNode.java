package assignment6;

public class ListNode {
	public Node first;//point to the first node
	public Node last; //point to the last node
	public int size = 0;//the size of the list
	
	//add the new node to list
	public void add(int data) {
		if(first == null) {
			first = new Node(data);
			last = first;
		}
		else {
			Node newA = new Node(data);
			last.next = newA;
			newA.previous = last;
			last = newA;
		}
		size ++;
	}
	
	//get the data of the entered position
	public int get(int index) {
		Node count = first;
		for(int i=1;i<index;i++) {
			count = count.next;
		}
		return count.data;
	}
	
	//get the size of the list
	public int size() {
		return size;
	}
	
	//remove the data of the entered position
	public void remove(int index) {
		Node temp = first;
		if(index == 1) {
			first = first.next;
			temp = null;
		}
		else {
			for(int i=1;i<index;i++) {
				temp = temp.next;				
			}
			temp.next.previous = temp.previous;
			temp.previous.next = temp.next;
			temp = null;
		}
		size --;		
	}
	
	//clear all the elements;
	public void clearAllElements() {
		first = null;
		last = null;
		size = 0;
	}
}
