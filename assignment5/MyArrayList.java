package assignment5;

public class MyArrayList {
	static private int INITIAL_CAPACITY = 8;
	
	private int[] arr = new int[INITIAL_CAPACITY];
	private int size = 0;
	
	public void add(int el) {
		
		if( size > arr.length * 0.75 ) {
			resize();
		}
		
		arr[size] = el;
		size++;
	}
	
	public int get(int index) {
		
		return arr[index];
		
	}
	
	public int size() {
		
		return size;
		
	}
	
	public void remove(int index) {
		if (index>size-1) {
			System.out.println("The index you input is out of the bound");
		}
		else {
			for(int i = index; i < size-1; i++ ) {
				arr[i] = arr[i+1];
			}
			size--;		
		}
	}
	
	public void clearALLElements() {
		arr = null;
		arr = new int[INITIAL_CAPACITY];
		size = 0;
	}
	
	private void resize() {
		
		int len = arr.length * 2;
		int[] newArr = new int[len];
		
		for(int i = 0; i < arr.length; i++) {
			newArr[i] = arr[i];
		}
		
		arr = newArr;
		
	}
}
