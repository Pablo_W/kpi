package assignment4;

import java.util.Scanner;

public class Fibonacci {
	
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt();
		System.out.println(iteration(x));
		System.out.println(recursion(x));
		System.out.println(tail_recursion(x));
	}
	
	//"for" loop
	public static int iteration(int x){
		if(x==1||x==2){
			return 1;
		}
		else {
			int a = 1;
			int b = 1;
			int c = 0;
			for(;x>2;x--){
				c = a+b;
				a = b;
				b = c;
			}
			return  c;
		}
	}
	
	//non-tail recursive
	public static int recursion(int x){
		if(x==1||x==2){
			return 1;
		}
		else {
			return recursion(x-1)+recursion(x-2);
		}
	}
	
	// tail recursive
	public static int tail_recursion(int x){
		return tail_recursion_helper(x, 1, 1);
	}
	
	private static int tail_recursion_helper(int x, int a, int b){
		if(x==1||x==2){
			return b;
		}
		else {
			return tail_recursion_helper(x-1, b, a+b);
		}
	}
	
}

