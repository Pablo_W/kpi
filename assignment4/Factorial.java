package assignment4;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stu
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt();
		System.out.println(iteration(x));
		System.out.println(recursion(x));
		System.out.println(tail_recursion(x));
	}
	
	//for loop
	public static int iteration(int x){
		int sum = 1;
		for(int i=1;i<=x;i++){
			sum *= i;
		}
		return sum;
	}
	
	//non-tail recursive
	public static int recursion(int x){
		if(x==1){ 
			return 1;
		}
		else {
			return x*recursion(x-1);
		}
	}
	
	//tail recursive
	public static int tail_recursion(int x) {
		return tail_recursion_helper(x,x,x-1);
	}
	private static int tail_recursion_helper(int x, int sum, int temp){
		if(x==1){
			return sum;
		}
		else {
			return tail_recursion_helper(x-1,sum*temp,temp-1);
		}
	}
	
}
