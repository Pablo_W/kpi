package main;

import school.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//initialize teacher Alex
		Dividing_Line.dividingStar();
		Teacher Alex = new Teacher("Alex", 20, 30, "male", 3000, "English");
		Alex.information();
		Alex.change_subject("Math");
		System.out.println();
		
		//initialize student Lily
		Dividing_Line.dividingStar();
		Student Lily = new Student("Lily", 15, "Female", "101", "Unknown", 3.1);
		Lily.information();
		Lily.choose_subject("Math");
		Lily.absence();
		Lily.ill();
		Lily.absence();
		Lily.attend();
		Lily.absence();
		Lily.change_classroom("202");
		Lily.up_GPA(2);
		Lily.down_GPA(1);
		System.out.println();
		
		//initialize subject Math
		Dividing_Line.dividingStar();
		Subject Math = new Subject("Math", 5, 5);
		Math.info_subject();
		Math.change_credit(6);		
		System.out.println();
		
		//initialize Classroom 202
		Dividing_Line.dividingStar();
		Classroom classroom = new Classroom(202, "general classroom");
		classroom.change_type("computer classroom");
		System.out.println();
		
		//initialize Staff Bob
		Dividing_Line.dividingStar();
		Staff Bob = new Staff("Bob", 33, 40, "male", 2000);
		Bob.information();
		Bob.salary_cut(200);
		Bob.salary_raise(1000);
		System.out.println();
		
		//initialize major subject History
		Dividing_Line.dividingStar();
		Subject History = new Major_Subject("History", 8, 5.0);
		History.info_subject();
		System.out.println();
		
		//initialize optional subject Music
		Dividing_Line.dividingStar();
		Subject Music = new Optional_Subject("Music", 10, 3.5);
		Music.info_subject();
		System.out.println();
		
		//initialize Dean John
		Dividing_Line.dividingStar();
		Staff Dean = new Dean("John", 35, 76, "Male", 8300, "08-09-2020 9am");
		Dean.information();
		Dean John = (Dean)Dean; 
		John.change_time("08-09-2020 8am");
		John.meeting();
		System.out.println();
		
		//initialize assistant Lucy
		Dividing_Line.dividingStar();
		Staff Lucy = new Assistant("Lucy", 20, 20, "Female", 4000);
		Lucy.information();
		System.out.println();
		
		//initialize Cleaner Tom
		Dividing_Line.dividingStar();
		Staff Cleaner =  new Cleaner("Tom", 37, 55, "Male", 3400, 0);
		Cleaner Tom = (Cleaner)Cleaner;
		Tom.add_allwance(200);
		Tom.information();
		System.out.println();
		
	}

}
