package school;

public class Teacher extends Staff{
    
	String teach_subject;   
    
    public Teacher(String name, int id, int age, String sex, double salary, String teach_subject) {
		super(name, id, age, sex, salary);
		this.teach_subject = teach_subject;
	}   
    
	@Override
	public String toString() {
		return "Teacher [name=" + name + ", id=" + id + ", age=" + age + ", sex=" + sex + ", teach_subject=" + teach_subject
				+ ", salary=" + salary + "]";
	}

	//teachers can change their subject 
    public void change_subject(String subject){
        teach_subject = subject;
        System.out.println("Teacher " + name + " changed his subject to " + subject + ".");
    }
    
}


