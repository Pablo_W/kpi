package school;

public class Subject{
    String name;
    int id;
    double credit;

	public Subject(String name, int id, double credit) {
		super();
		this.name = name;
		this.id = id;
		this.credit = credit;
	}

	@Override
	public String toString() {
		return "Subject [name=" + name + ", id=" + id + ", credit=" + credit + "]";
	}

	public void change_credit(double credit){
        this.credit = credit;
        System.out.println("Attention! " + name + "'s credit is changed to " + credit + ".");
	}
	
	public void info_subject(){
		   System.out.println(toString());
		   Dividing_Line.dividingLine();
		   }
	
}


