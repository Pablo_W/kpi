package school;

public class Student{
    String name;
    int age;
    String sex;
    String classroom;
    String subject;
    String absence = "No";
    double GPA;

    public Student(String name, int age, String sex,
                   String classroom, String subject, double GPA){
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.classroom = classroom;
        this.subject = subject;
        this.GPA = GPA;
        this.absence = "Unknown";
    }   
    
    @Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", sex=" + sex + ", classroom=" + classroom + ", GPA=" + GPA
				+ "]";
	}
    
    public void information(){
        System.out.println(toString());
        Dividing_Line.dividingLine();
    }
    
	public void choose_subject(String subject){
        this.subject = subject;
        System.out.println("Student "+name+" chose "+subject+" successfully." );
    }
	
    public void ill(){
        absence = "Yes";
    }
    
    public void attend(){
        absence = "No";
    }
    
    public void absence(){
    	if(absence.equals("Yes")){
    		System.out.println(name+" is absent.");
    	}
    	else if(absence.equals("No")) {
    		System.out.println(name+" is present.");
		}
    	else {
    		System.out.println(name+"'s information of absence is unknown.");
		}
    	
    }
    
    public void change_classroom(String classroom){
        this.classroom = classroom;
        System.out.println(name+" changed the class room to " + classroom + ".");
    }
    
    public void up_GPA(double value){
        GPA = GPA + value;
        System.out.println("Now "+name+"'s GPA is "+GPA+",congratulation!");
    }
    
    public void down_GPA(double value){
        GPA = GPA - value;
        System.out.println("Now "+name+"'s GPA is "+GPA+",don't give up！");
    }
    
}

