package school;

public class Cleaner extends Staff{
	   	
	double allowance;
	
	public Cleaner(String name, int id, int age, String sex, double salary, double allowance) {
		super(name, id, age, sex, salary);
		this.allowance = allowance;
	}
	
	@Override
	public String toString() {
		return "Cleaner [name=" + name + ", id=" + id + ", age=" + age + ", sex=" + sex + ", salary=" + salary
				+ ", allowance=" + allowance + "]";
	}

	@Override
	public void information(){
	    System.out.println(toString());
	    Dividing_Line.dividingLine();
	}
	
	public void add_allwance(double money){
		allowance = money;
		salary += allowance;
	}
		
}

