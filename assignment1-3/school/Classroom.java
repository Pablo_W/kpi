package school;

public class Classroom{
    int number;
    String type;

    public  Classroom(int number, String type){
        this.number = number;
        this.type = type;
    }

    public void change_type(String type){
        this.type = type;
        System.out.println("Room"+number + " has already changed to " + type +".");
    }
}

