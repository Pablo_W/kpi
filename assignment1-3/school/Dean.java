package school;

public class Dean extends Staff{
	   	
    String meeting_time;
 	
	public Dean(String name, int id, int age, String sex, double salary, String meeting_time) {
		super(name, id, age, sex, salary);
		this.meeting_time = meeting_time;
	}
	
	@Override
   	public void information(){
		System.out.println("This is our dear dean.");
        System.out.println(toString());
        Dividing_Line.dividingLine();
    }
	
	public void meeting(){
		System.out.println("!!!");
		System.out.println("Attenion, we will have a meeting. The time is " + meeting_time + ".");
	}
	
	public void change_time(String new_time){
		meeting_time = new_time;
		System.out.println("!!!");
		System.out.println("Attenion, The meeting time has changed. The time is " + meeting_time + ".");
	}
	   
}

