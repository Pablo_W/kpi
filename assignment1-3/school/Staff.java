package school;

public class Staff{
    String name;
    int age;
    int id;
    double salary;
    String sex;


    public Staff(String name, int id, int age, String sex, double salary) {
		this.name = name;
		this.id = id;
		this.age = age;
		this.sex = sex;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Staff [name=" + name + ", id=" + id + ", age=" + age + ", sex=" + sex + ", salary=" + salary + "]";
	}
	
	public void information() {
		System.out.println(toString());
		Dividing_Line.dividingLine();
	}

	public void salary_raise(double value){
        salary += value;
        System.out.println(name+"'s salary has raised to "+salary);
    }
    
    public void salary_cut(double value){
        salary -= value;
        System.out.println(name+"'s salary has cut to "+salary);
    }  
    
}

